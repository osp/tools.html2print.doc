Title: OSP @ HEAD, Strasbourg
Date: 2017-09-13
Translation: true
Lang: fr
Tags:

## OSP @ HEAR, Strasbourg

## Open Source Publishing

## [http://osp.kitchen/](http://osp.kitchen/)

Depuis 2006, Open Source Publishing pratique le design graphique avec des logiciels libres et open source.

Nous sommes aujourd'hui à Bruxelles, 8 femmes et hommes venant de différents domaines.

## Pourquoi le logiciel libre?

*   Pour pouvoir modifier des choses qui ne nous conviennent pas forcément dans nos outils quotidiens
*   Pour expérimenter de nouveaux outils
*   Pour utiliser des logiciels dans un autre but que celui d'origine
*   Ne pas tomber dans une routine de confort
*   …

## Freedom 0

<figure>![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040639rr.jpg)

<figcaption>

The freedom to run the program as you wish, for any purpose

</figcaption>

</figure>

## Freedom 1

<figure>![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040640rr.jpg)

<figcaption>

The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1). Access to the source code is a precondition for this.

</figcaption>

</figure>

## Freedom 2

<figure>![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040641rr.jpg)

<figcaption>

The freedom to redistribute copies so you can help your neighbor (freedom 2)

</figcaption>

</figure>

## Freedom 3

<figure>![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040642rr.jpg)

<figcaption>

The freedom to distribute copies of your modified versions to others (freedom 3). By doing this you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.

</figcaption>

</figure>

## Rule 0

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040643rr.jpg)

## Rule 1

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040644rr.jpg)

## Print party

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.003.jpg)

## Print party

Pour partager notre pratique, nous organisons des _Print Parties_. Des événements publics où nous produisons en _live_ un objet graphique avec des logiciels libres et open source.

À côté des ordinateurs, nous cuisinons en même temps. Imaginez une recette de cuisine non-libre:

*   on ne pourrait pas demander les ingrédients (interdit par la loi!);
*   on ne pourrait pas la refaire;
*   on ne pourrait pas la partager;
*   on ne pourrait pas la modifier (même s'il manque un ingrédient).

## Nous préférons dire que nous sommes une cuisine.

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.002.jpg)

## Nos recettes

<iframe src="http://osp.kitchen"></iframe>

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.030.jpg)

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.031.jpg)

## Visual Grammar

## Visual Grammar

<figure>![](http://osp.kitchen/api/osp.work.visual-grammar/6d87e02a95840d0e15a0ada729a14ea6abe28c7b/blob-data/screenshot_14-07-12_15-58-51.png)

<figcaption>

Text by Letterror, "Bézier: Prefab Smoothness", in _Letterror_, September 1989

</figcaption>

</figure>

## Visual Grammar

![](http://osp.kitchen/api/osp.work.visual-grammar/9ce886d956c8f223e505b31dba8b6071799ce848/blob-data/osp-visual-grammar-poster.png)

## Visual Grammar

### → Démo ←

## Pantographe

## Pantographe

<figure>![](http://ospublish.constantvzw.org/images/var/resizes/Up-Pen-Down-December/IMG_1843.JPG)

<figcaption>

Par les étudiants de l'ÉSAD •Valence

</figcaption>

</figure>

## Pantographe @ la Balsamine

<figure>![](http://ospublish.constantvzw.org/images/var/resizes/Bootstrap-IV/DSCF2653_small.jpg)

<figcaption>

Au théâtre la Balsamine

</figcaption>

</figure>

## Pantographe @ la Balsamine

<iframe src="https://player.vimeo.com/video/64151382" webkitallowfullscreen="" mozallowfullscreen="" width="500" frameborder="0" allowfullscreen="" height="281"></iframe>

[Bootstrap IV — Print Party — Théâtre de la Balsamine](https://vimeo.com/64151382)

## Literal Draw

## Literal Draw

### Qu'est-ce qu'un dessin digital?

Un logiciel de dessin vectoriel pédagogique qui montre trois représentations d'un dessin digital.

## Literal Draw au Vietnam

![](http://osp.kitchen/api/osp.tools.literaldraw/7081a65f1cb19d3ef1e97b0f8219a94b1905c019/blob-data/vietnam1.JPG)

## Literal Draw au Vietnam

![](http://osp.kitchen/api/osp.tools.literaldraw/ccbf64fb37ccdfbadaef09bc101ee2341ad1c71f/blob-data/vietnam2.JPG)

## Literal Draw au Vietnam

![](http://osp.kitchen/api/osp.tools.literaldraw/8881df1d929983fa1790c882f847383dd3fe2ae5/blob-data/vietnam5.JPG)

## Literal Draw au Vietnam

![](http://osp.kitchen/api/osp.tools.literaldraw/9a16e9216ce9c416615d7d0892cd29610cda94f4/blob-data/vietnam4.JPG)

## Literal Drawing @ ERG

![](http://osp.kitchen/api/osp.tools.literaldraw/2c855f6caa4a9fce8cd1aeeaa0d7df6c10216d87/blob-data/pierre-barick_fogue-kake.png)

## Scribus IRC

## Scribus IRC (Internet Relay Chat)

<figure>![](http://ospublish.constantvzw.org/blog/wp-content/uploads/screenshot_09-03-11_18-14-22.png)

<figcaption>

We connected the output of an IRC channel to the python scripting interface of Scribus, enabling several people to input commands at the same time, which is 'the closest' we got to collaborative drawing / designing.

</figcaption>

</figure>

## Scribus IRC (Internet Relay Chat)

<figure>![](http://osp.kitchen/visual/osp.tools.scribus-irc/8f1b1ca0ae9018265484592543fc290f74e77d3f/width..1000/p1050766.jpg)

<figcaption>

History of the poster

</figcaption>

</figure>

## Scribus IRC (Internet Relay Chat)

![](http://osp.kitchen/visual/osp.tools.scribus-irc/93e92a379162594a02e23b482db1ff6ce68d5a04/width..1000/p1050769.jpg)

## html2print

## html2print

<figure>![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.011.jpg)

<figcaption>

Utilisation d'Etherpad pour un design collaboratif

</figcaption>

</figure>

## html2print

<figure>![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/vj14_spread-in-browser.png)

<figcaption>

Double-pages dans le navigateur

</figcaption>

</figure>

## html2print

<figure>![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/vj14_chemin-de-fer-in-browser.png)

<figcaption>

Chemin de fer dans le navigateur

</figcaption>

</figure>

## html2print

<figure>![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/vj14_pdf.png)

<figcaption>

Export PDF

</figcaption>

</figure>

## html2print @ HEAR

## html2print @ HEAR

![](http://comgraph.hear.fr/wp-content/uploads/2015/03/bcb-1-1024x679.jpg)

## html2print @ HEAR

![](http://comgraph.hear.fr/wp-content/uploads/2015/03/bcb2.jpg)

## html2print @ HEAR

### → Démo ←

## html2print & Médor

<iframe src="https://medor.coop" style="background-color: white;"></iframe>

## html2print & la Balsamine

![](http://osp.kitchen/api/osp.work.balsamine.2014-2015/raw/iceberg/numbering-in-javascript.png)

## Laid out

## Laid out

### Software by Tom Lechner, Portland

### → http://laidout.org ←

## Laid out

### Un article d'OSP à propos de Laid Out pour Grafik

<figure>![](http://i.imgur.com/M82QAbI.png?1)

<figcaption>

http://www.grafik.net/category/screenshot/diy-software

</figcaption>

</figure>

## Laid out

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/laidout-grafik-tools.png)

## Laid out

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/out-1_762-1327.gif)

## Laid out

![](http://osp.kitchen/api/osp.work.balsamine.2014-2015/20099da9d3900d8632ddb1f9da2811962432e23f/blob-data/cerebrum3sharp.png)

## Laid out

<embed src="http://osp.kitchen/api/osp.work.balsamine.2014-2015/7880703f84c797231fe3b8fc247e886968b2a49c/blob-data/cerebrum-serie2.svg">

## Laid out

### → Démo ←

## Use our tools

### → http://osp.kitchen/tools/ ←
