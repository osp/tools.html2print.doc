Title: Medor Plateform
Date: 2017-09-13
Translation: true
Lang: fr
Tags:

## medor-plateform.md

## Plateforme Médor

### Contexte

Le projet Médor s'est construit autour de l'idée que la structure qu'il mettrait en place serait la garantie d'un journalisme de qualité, permettant tout du moins de favoriser son émergence ainsi que celle de nouvelles formes d'écritures graphiques et textuelles. Cette structure passe par une série d'outils de gouvernance et d'organisation de la rédaction pour beaucoup numériques. Et puisque les solutions logicielles dominantes telles qu'Adobe supportent et renforcent des modes d'organisation (verticale) et de création (solitaire et _gutenbergiennes_) desquels Médor essaye de s'extraire, il nous a semblé important d'inventer notre propre flux de travail. Médor a donc décidé de consacrer une partie de son budget à l'élaboration d'une plateforme d'édition et de mise en page qui proposerait d'autres processus plus en phase avec ses principes et ambitions. Avec en avant plan une collaboration plus étroite entre les différents acteurs de la chaîne d'édition.

### Choix

OSP a fait le choix des logiciels libres ou _open source_. Parce qu'ils font partie intégrante de notre engagement et parce qu'ils nous offrent une diversité d'approches plus riche que celle des logiciels propriétaires, ces outils nous permettent de penser autrement le design — dans sa forme mais aussi au niveau de l'organisation du travail. La mise en page est un domaine complexe qui demande à faire cohabiter des éléments hétérogènes (typographie, dessin vectoriel, images bitmap, etc.). Pour la composition, deux approches coexistent. Un certain nombre de logiciels sont programmatiques, très appropriés pour gérer des flux mais difficiellement malléables pour des contenus mixtes et donc peu adaptés à la mise en page de magazines. Avec une approche avant tout visuelle Scribus est un programme qui suit lui un principe de canvas, proche de celui d'InDesign, mais qui reste contraint et peu dynamique en terme de design et de processus.

Depuis quelques années, les technologies du web avancent à grands pas et permettent de plus en plus de finesses typographique. Ces technologies sont pérennes, ouvertes, hyper-documentées et malléables. Elles invitent à l'appropriation pour construire de nouvelles interfaces et flux de travail. La mise en page HTML pour le papier en est encore à ses balbutiements, mais fort de notre expérience sur des projets plus restreints que Médor nous avons voulu parier sur ces technologies. Médor n'est-il pas le projet idéal pour repousser les limites?

### État des lieux

Au cours de la première année d'existence de Médor, nous nous sommes principalement concentrés sur la plateforme d'édition et de mise en page. Elle se traduit concrètement par une série d'outils logiciels pour l'édition, la mise en page pour le papier, web et les pratiques -et procédures- qui les accompagnent. Elle se développe de manière incrémentale et itérative, en fonction des besoins prioritaires mais aussi des opportunités offertes par les sujets ou projets satellites.

3 pôles se distinguent:

1.  Plateforme d'édition
2.  Plateforme de mise en page pour le papier
3.  Plateforme de mise en page web

#### Plateforme d'édition

La plateforme d'édition permet de:

*   créer des articles
    *   leur associer des personnes avec différents rôles (auteurs, illustrateurs, marraines)
    *   leur associer des licences, dates de publication et autres méta-données
    *   définir des rubriques associées aux articles
    *   importer “proprement” des contenus depuis Word ou Libre Office
    *   éditer les articles au format HTML, via une interface WYSIWYG ou via le code source, en utisant des styles de formattage prédéfinis
    *   téléverser, organiser et insérer des visuels dans les articles
    *   classer et trier par type d'article
    *   garder un historique de ces articles
*   créer une table des matières par numéro
    *   calculer automatiquement la pagination et le titre courant des articles
    *   publier le sommaire en ligne

Additionellement, la plateforme d'édition expose l'ensemble des contenus d'un numéro de manière structurée afin qu'un programme tiers puissent rećupérer ou modifier les contenus programmatiquement, le tout de manière sécurisée.

Si la plateforme est à l'heure actuelle assez proche d'un CMS classique, la possibilité de s'y brancher à distance à l'aide d'outils variés offre cependant des perspectives intéressantes pour développer des interfaces plus complexes. La plateforme de mise en page est en quelque sorte la premières partie d'un ensemble d'outils ou d'extensions plus larges.

#### Plateforme de mise en page

La plateforme de mise en page consiste en:

*   une série de scripts permettant de récupérer et de structurer les contenus de la plateforme
*   un logiciel de correction typographique pour les erreurs les plus courantes (espaces et ponctuations)
*   un ensemble de scripts et conventions pour permettre la mise en page dans un navigateur web
*   un navigateur web spécifique (OSPKit) assemblé par nos soins pour accèder aux fonctionnalités les plus récentes du web tout en se déchargeant de fonctionnalité moins nécessaires à notre destination imprimée
*   un ensemble de feuilles de style définissant l'identité graphique et les comportements de la composition du magazine
*   un ensemble de scripts pour rendre les fichiers compatibles avec un système d'impression offset traditionnel (avec ou sans surimpression du noir)

L'ensemble de ces outils d'édition et de mise en page se sont construit entre septembre 2015 et juin 2016\. Dorénavant en place et effectifs, ils nous ont permis de mettre en forme les 4 premiers numéros de Médor, non sans efforts et endurance mais avec aujourd'hui plus de fluidité.

#### Plateforme web

Il est actuellement déjà possible de publier les articles papier en ligne. Les principaux challenges restent:

*   le placement des images et encadrés puisque la mise en place de ces éléments se fait manuellement pour la version papier. Ils sont parfois encodés en fin d'articles et non dans le corps du texte à l'endroit où ils sont supposés apparaître. Lors de la publication en ligne, leur placement est donc problématique mais ajustable par corrections avant publication web, ou en se contraignant à déjà prendre en compte systématiquement le web lors de la mise en forme pour le papier.
*   la compression automatiques des images puisque la dernière version des articles contient des liens vers des images haute définition destinées à l'impression. Le poids de ces images n'est pas approprié pour l'affichage web. Une version plus légère de chacun des visuels pourrait être appelées automatiquement.
*   la possibilité de conserver des styles particuliers développés pour la mise en page papier,. Nous avons des styles généraux par type d'articles mais décrivons aussi des styles particuliers (ex. style d'encadrés) qui ne sont en l'état pas reportés sur le web.
*   l'objectif d'avoir plusieurs versions d'un même articles puisqu'actuellement, les contenus web et imprimés sont identiques. Il n'est pas possible, sauf en dupliquant les articles, de proposer par exemple une version longue d'un papier en ligne

## Développement futur

La budget plateforme étant planifié comme utilisable à partir de l'année 2, le travail nécessaire à la construction des 4 premiers numéros effectué au cours de l'année 1, n'a pas été rémunéré mais s'est fondu dans le partage des heures de travail du «Pilote de look» (3735€), incluant direction artistique, coordination et mise en page. Ce travail est cependant parvenu à se consolider même si des ajustements sont possibles. Du côté d'OSP, nous avons rencontré un réel intêret de la part de pairs, et souhaitons communiquer sur ce travail afin de susciter de l'adhésion et d'attirer des contributions externes. Nous travaillons par exemple avec le centre/école d'art La Villa Arson en se basant sur le travail mis en place pour Médor (lui-même basé sur des travaux plus anciens). En retour, nous pensons que ce travail tiers bénéficiera à Médor.

Les logiciels libres, comme de plus en plus de logiciels privatifs développés à travers des méthodes dites "agiles", sont généralement le fruit d'un dialogue entre prévision et décisions ad hoc. Plutôt que de tout prévoir 2 ans à l'avance, nous préférons lister ici des chantiers possibles pour ensuite définir les priorités avec le pôle rédactionnel. Ci-dessous donc une proposition de _timeline_ pour les chantiers «plateforme» à venir, à affiner ensemble donc.

### Prochaines étapes de la plateforme

Classée par priorité avec une estimation calendrier, elles mesurent aussi la difficultés envisagée

#### En continu

*   exploration des dernières possibilités CSS pour construire des mises en pages plus singulières qui tirent parti des affordances du HTML/CSS, plutôt que de reproduire des mises en pages classiques de magazines
*   développement du navigateur OSPKit

#### Mars 2017

*   améliorations de l'éditeur de textes (aperçu des styles)
*   meilleure gestion des césures
*   installation d'html2print sur le serveur pour que les pilotes puissent voir la mise en page au fur et à mesure de sa construction
*   meilleur intégration du gestionnaire d'images avec l'éditeur de texte

#### Juin 2017

*   experimentations autour de la possibiliter de textes justifiés de qualité
*   expérimentation de placement (semi-)automatique des notes de bas de page
*   interface graphique permettant de manipuler les objets sur la page
*   visualisation et manipulation du sommaire sous forme de chemin de fer
*   possibilité de générer des PDFs à la volée pour aperçu en temps réel de la mise en page

#### Janvier 2018:

*   penser l'articulation web-imprimé (comment un article peut être sur le web en version augmentée/diminuée sans couper les liens avec la mise en page papier)
*   réfléchir à comment échapper à la question du _template_, c'est à dire de pouvoir publier des articles à la forme spécifique sur le web.

#### En continu, peut-être ou pour plus tard (et sur un autre budget):

Ici, des chantiers énoncés lors dans le dossier que nous avions soumis à Boost-Up, mais dont l'ampleur dépasse le budget alloué.

*   possibilité d'éditer le contenu dans la mise en page (dépend de l'implémentation correcte d'une spécification CSS)
*   permettre aux éditeurs de commenter leurs changements, de commenter le texte même et idéalement aussi de confirmer ou rejeter des changements
*   collaboration en temps réel et asynchrone
*   Gestion des sources et de leur provenance
*   Gestion des droits d'accès et d'écriture
*   renforcement de la sécurité
*   Penser l'édition des articles
    *   suggestions de corrections dans l'éditeur de texte
    *   possibilité de _peer-review_ des articles
    *   édition des textes hors-ligne
    *   plusieurs modes d'écriture (public/privé - à plusieurs mains ou à une main)
    *   versions commentées (_annotateit_)

## Conclusion

Considérant cette _timeline_, nous prévoyons de facturer ce jour une première partie du budget projeté pour la plateforme dans le plan financier de départ. Cette première partie permettra de rétribuer les étapes déjà franchies à hauteur d'un montant de 7000 euros.

Une réserve de 7000 euros sera utilisée pour la mise en place des prochaines étapes.

## Liens

*   https://medor.coop/cuisine/index.php/M%C3%A9dor_sauce_html
*   http://osp.constantvzw.org:9999/p/medor-boost-up
*   http://osp.constantvzw.org:9999/p/medor-tools

## Images

### Plateforme d'édition

<figure>![liste article](https://osp.clouddiskspace.nl/index.php/s/7yDm2bAfo5APWZq/download)

<figcaption>

Liste des articles. Des options permettent de filtrer les articles par numéro et statut.

</figcaption>

</figure>

<figure>![editeur de texte](https://osp.clouddiskspace.nl/index.php/s/aPtKWfe0zhkre1z/download)

<figcaption>

Éditeur d'articles. Les options de l'éditeur sont adaptées à la charte graphique de Médor.

</figcaption>

</figure>

<figure>![Sommaire](https://osp.clouddiskspace.nl/index.php/s/RwEKAP8VBeYnGtz/download)

<figcaption>

Gestion du sommaire des numéros. Cette interface permet de sélectionner et d'organiser le contenu d'un numéro. À posteriori, elle permet de publier en ligne le sommaire sur le site.

</figcaption>

</figure>

<figure>![JSON](https://osp.clouddiskspace.nl/index.php/s/raWBCfc7eAvC48l/download)

<figcaption>

Interface présentant le contenu des numéros sous forme structurées (API). Elle permet l'export et l'import programmatique de contenus.

</figcaption>

</figure>

<figure>![JSON2](https://osp.clouddiskspace.nl/index.php/s/d3LPxCGsUr8Ryrn/download)

<figcaption>

Détail de l'interface programmatique pour le numéro quatre.

</figcaption>

</figure>

<figure>![Gestionnaire d'images](https://osp.clouddiskspace.nl/index.php/s/ntfh6eZ3QWWylgT/download)

<figcaption>

Gestion des images sur le serveur medor.coop

</figcaption>

</figure>

### Plateforme de mise en page

<figure>![OSPKit](https://osp.clouddiskspace.nl/index.php/s/Ygw53slzn81NdCg/download)

<figcaption>

OSPKit: le navigateur assemblés par OSP pour un meilleurs support des fonctionnalités nécéssaires à la mise en page.

</figcaption>

</figure>

<figure>![OSPKit print](https://osp.clouddiskspace.nl/index.php/s/6B27A2CZCdy7kAF/download)

<figcaption>

Impression d'articles dans OSPKit

</figcaption>

</figure>

<figure>![OSPKit + inspector](https://osp.clouddiskspace.nl/index.php/s/fnXuTbdvfKvEMwR/download)

<figcaption>

Vue d'OSPKit avec l'inspecteur de code. Il permet d'ajuster les feuilles de styles et de voir le résultat instantanement.

</figcaption>

</figure>

<figure>![Bugreport webkit](https://osp.clouddiskspace.nl/index.php/s/jYGX30ziUAICsoA/download)

<figcaption>

Exemple de contribution au moteur de rendu d'OSPKit (rapport d'erreur)

</figcaption>

</figure>

<figure>![HTML5Lib Typogrify](https://osp.clouddiskspace.nl/index.php/s/w477EMlGeVjRzNA/download)

<figcaption>

Page d'accueil du projet d'enrichissement typographique créé pour Médor

</figcaption>

</figure>

### Questions de Céline pour la plateforme

#### Qui a pris/prend quoi en charge dans ce travail de réalisation de la plateforme : OSP, les membres Médor-OSP, Steph et Alex?

Comme présenté ci-dessus, la plateforme est constituée de plusieurs parties logicielles.

Html2print a été développé en amont du projet Médor par Stéphanie et Alexandre en amont du projet Médor. Il a été et est utilisé depuis dans nombre de projets d'édition menés par OSP mais aussi et de plus en plus par d'autres, principalement étudiants et/ou studio proches ou connectés au réseau OSP. exemples :

OSPKit est développé par Stéphanie et Alexandre

Le Django CMS Médor a jusqu'à présent été principalement développé par Alexandre. Sarah et Ludi en étant les premières utilisatrices, elles ont activement contribuées à sa construction et ses ajouts.

#### Quelle est la nature de ce travail (programmation, autre?)?

*   modelisation d'un workflow
*   mise en place de procédures et conventions pour accompagner ce workflow
*   implementation logicielle de ce workflow
*   assemblage de modules existants
*   programmation de pièces manquantes
*   documentation

#### Peut-on avoir une idée de l'ampleur du travail (combien de personnes pendant combien de semaines depuis quelle année pour aboutir au résultat actuel) à la très grosse louche?

La mise en place de ces outils a démarré en mai 2015. On compte actuellment

#### Qui est l'organisateur de tout ce travail et qui se charge de mobiliser les médoriens (pilotes, com', presse, etc) ou des énergies extérieures pour assurer une résonance maximale au projet?

#### Qui édite la facture?

L'association OSP. C'est le buffer du projet. C'est elle qui investit le temps et l'énergie de ses membre dans les avancementset prise de rique des outils.

#### Pouvez-vous détaillez précisément les prestations réalisées (comme pour une facture)?

*   OSPKit
*   HTML2print https://medor.coop/cuisine/index.php/M%C3%A9dor_sauce_html
*   CMS Médor

#### A qui appartient cette plateforme (si on arrête Médor demain)?

Les projets sont diffusés sous licence libre, sous licence GPL ou AGPL selon les cas. Concrètement, celà signifie q

#### Qui en possède les clés? Où est-elle stockée? Comment est-elle partagée?

L'ensemble des fichiers est d'ores et déjà publié sur gitlab.constantvzw.org:

*   http://gitlab.constantvzw.org/osp/tools.html2print
*   http://gitlab.constantvzw.org/osp/tools.ospkit
*   http://gitlab.constantvzw.org/osp/work.medor.www

Chacun peut télécharger les fichier, et même les republier ailleurs Un effort de documentation est nécessaire pour faciliter la prise en main des outils

#### Qui est capable de s'en servir si Alex part sur la Lune?

Pour HTML2print: personnes avec un profil webdesigner Pour OSPkit: personnes qui savent installer un logiciel Pour la plateforme Médor: connaissance de Django (de la même maniére que quelqu'un qui veut installer wordpress doit connaitre wordpress)

#### Quel est, par exemple, l'intérêt de Steph de travailler là-dessus (la plateforme est-elle utilisée dans les autres travaux d'OSP?)?

Stéphanie et l'ensemble d'OSP n'utilise pas toute la plateforme, mais seulement certaines parties spécifiques: HTML2print et OSPkit. Le but est de chambouler les pratiques Html2Print est actuellement utilisé pour la mise en page de la communication des projets suivants :

Balsa ESAD GV (proto HTML2print using context) Villa Arson (diplomés 2015 + plateforme)

The riddle of the real city avec 1001 Publisher et Institute of Networked Cultures Anna Kavan Adva

Relearn (book sprint) Home Tourism (book sprint) What's the matter with cooperation (book sprint)

Le CMS est spécifique à Médor.

#### Quelles mesures ont été prises (ou vont être prises: dans ce cas, avec quel budget et quel timing?) pour qu'un plus grand nombre de personnes puissent se l'approprier, l'améliorer, l'utiliser ? Autrement dit: comment lutte-t-on contre la dépendance à Alex?

mise en place d'une liste de discussion: https://listes.domainepublic.net/admin/html2print Cette liste de discussion à pour but de rapprocher les différentes personnes ayant manifesté de l'intéret pour l'outil ou utilisant des processus similaires.

#### Quelles mesures ont été prises (ou vont être prises: dans ce cas, avec quel budget et quel timing?) pour diffuser les fruits de ce travail dans un réseau belge et international de médias ou organisations qui pourraient être intéressés?

La demande du subvention via le projet Boost-up / Industrie Créative n'a pas aboutie et ses critères semblent en décalage par rapport à la démarche OSP/Médor. Ils sont basés sur le "lancement sur le marché d'un prototype" et la "rentabilité qui en découle " plutôt que sur la recherche et le développement d'un outil. Les outils et leurs enjeux étant maintenant plus clairs aussi pour l'équipe fondateurs et communication, ils serait opportun d'engager un travail de communication plus régulier à leur sujet (post à chaque numéro). Plusieurs présentation de l'outilsont eu lieu :

LGM 2015 LGM 2016

Associalibre, Anderlecht, octobre 2016 → http://2016.associalibre.be/?Medor-un-magazine-trimestriel-belge-et-cooperatif-qui-utilise-du-libre

Papier Carbone, Charleroi, décembre 2016 http://www.bps22.be/fr/Evenements/PAPIER-CARBONE

Signes Paris, février

Mailing list HTML2print

#### Peut-on imaginer en retirer de l'argent? Si oui, comment?

Dans le modèle logiciel classique on "loue" le droit d'utiliser le logiciel. Ici ce n'est pas le cas puisque le logiciel est libre, et donc libre d'être utilisé.

Si on souhaitait en retirer de l'argent, ce serait plutôt sous forme d'expertise: étant les initiateurs du projet, nous avons un crédit plus important. Nous pourrions imaginer proposer nos services a d'autres organisations avec des besoins similaires. Il ne s'agit pas de vendre un produit clés en main, mais plutôt de se baser sur un enseble de briques logiciels et de pratiques pour créer des solutions sur-mesures.

Un exemple: Nous travaillons actuellement pour la villa Arson pour leur permettre

#### Comment Médor peut-il profiter dès aujourd'hui de cet investissement en termes de communication?

En communiquant plus régulièrement sur ses outils au moment des interviews, rencontre, posts Facebook,etc (Avec des blagues sur les workflow classiques et systèmes de versionnements Words?)

#### En quoi est-ce exceptionnel comme travail? Cela intéresse qui?

Le kit outils peut bénéficier à d'autres projets magazine et ou de publication. L'interface d'administration des numéros, articles, images (Django CMS Médor) peut convenir à tout type de publication. Les outils de mise en page en sortie peuvent différer. Un branchement avec des outils privatifs en sortie est possible, par exemple via un export xml, le contenu pourrait être mis en page de manière synchronisée avec InDesign.

#### Peut-on avoir des engagements sur des résultats concrets (une obligation de résultat) ou Médor finance-t-il un travail de recherche sans être sûr que celles-ci aboutissent à un résultat (les termes "exploration", "expérimentation", "réflexion" etc; donnent une impression de flou : savez-vous exactement où vous allez?)?

Le résultat concrets c'est 5 numéros qui sont sortis du four.

Que les journalistes puissent voir la mise en page de manière autonome

#### Le budget prévu garantit-il un résultat minimal satisfaisant?

#### Sinon, quelles sont les sources de financement possibles? Qui se charge de les mobiliser (budget, timing)?

#### Peut-on avoir des garanties à plus court terme sur la sécurité? Que manque-t-il pour sécuriser cette plateforme?

La sécurité dépend de la sécurité du serveur. Celui-ci est raisonnablement sûr: certificat ssl

https://www.ssllabs.com/ssltest/analyze.html?d=medor.coop&hideResults=on&latest

cependant Alex n'est en rien expert en sécurité, et ne veux pas prendre de responsabilités à ce niveau lá. Pour la

### Pour janvier

*   finir la note
*   mettre sur le wiki
*   la faire relire à Laurence
*   La mettre à l'ODJ du CA de Janvier

### En général

*   annoncer les événements liés à ça (ex. signes Paris, mailing-list)
