Title: html2print Pecha Kucha
Date: 2017-09-13
Translation: true
Lang: fr
Tags:

## html2print-pecha-kucha

1 # OSP, osp is a collective of graphic designers using only free and open source software. We experiment with the tools we use and the way we work.

2 # Free culture can be explained by 4 freedoms and 2 obligations. We like the analogy of cooking as it explains the necessity.

3 # Freedom 0, use it . like you are allowed to eat a jam. We use many different kinds of software . Also tools that you might not expect graphic designers would use

4 # We've tried many kinds of different software , many are open source variants of traditional tools in a graphic design practice. But we also like to use tools developed in a different background.

5 # For the Brussels theater, balsamine, we used context, a typesetting tool, mostly used by scientists. And graphviz, a tool to visualize graphs developed by AT&T .

6 # For the same theater we also tried to make graphic design using a spreadsheet. We were interested how this tool would influence the design we made.

7 # Because we believe the tools shape the practice. But obviously our tools are also shaped, formed and informed by our practice. Practice shapes tools shapes practice.

8 # Freedom 1 , study it : you are allowed to understand how a jam was made. More and more technology is being closed and it s workings hidden.

9 # Metahershey, a font based on the hershey font collection, interpreted by metafont and drawn using a pen plotter. All through pre-90's technology, 3 years ago.

10 # A positive exception to modern closing tech is html. Through the inspector in the browser it's possible to study & dissect any site. A lot of knowledge available onine.

11 # Freedom 2 , Modify it. When you understand, but see where it can be improved, better suited to your context you are allowed to. Like this slide, adapted to a Korean context.

12 # It can be a radical treatment. Like this, where we used an algorithm to find the skeleton of a letter. And even tried to redress it a bit.

13 # Or it's a subtle transformation, like our tool html2print which allows to design printed publication using web technologies. Next to a different aesthetic it affords a different way of working.

14 # By editing the style sheet in an etherpad collaborative design all of a sudden becomes possible. By combining existing technologies new exiting ideas are accesible.

15 # Freedom 3 , share it . If you found a different recipe you can share it . It's perhaps the most important part of our practice. Especially as we like to share more than just our tools. Our whole way of working.

16 # In workshops & the summerschool relearn, where we, inspired by the switching roles of student and teachers in open source world are looking for active engagement, and try to learn ourselves.

17 # Or through printparties : public events that allow to share a part of our practice with a larger audience in a performative way . [ The cooking and eating of a dish is an crucial part of such an event . And in the end they can take the recipe home . ]

18 # We end with the two obligations to ensure this freedoms . You need to acknowledge the work of others you build upon, and you are not allowed to close it again.

19 # We try to acknowledge the work of others by include=ing a readme on our website and by naming the technologies we used in the colophon of our publications. And of course we publish our work under a Free & open license. All our work is available on our website. Wich is in fact a visualization tool for our Git repositories.

20 # So we welcome you to visit osp.kitchen. Study, modify & share our work. Or contact us if you are enticed for a collaboration. *http://ospublish.constantvzw.org/blog/wp-content/uploads/out-1_762-1327.gif http://ospublish.constantvzw.org/images/var/albums/LGM-2013/DSCF3668_s.jpg?m=1396269170

![](http://ospublish.constantvzw.org/images/var/albums/LGM-2013/DSCF3749_s.jpg?m=1396269175)

![](http://osp.kitchen/visual/osp.work.balsamine.2013-2014/cfe79eae30879f1a0e2bbb9e629d59a9627e83f3/width..1000/screenshot_2013-05-04_15.png)

![](http://osp.kitchen/visual/osp.work.balsamine.2013-2014/cc13f82ce8199fe8dda5cc6eb429a7c77fedfab3/width..1000/pmwiki-hotglue-howto-02.png)

![](http://osp.kitchen/visual/osp.work.balsamine.2014-2015/8fe69db1e8ed1c51c7d011b4098427b7243abbac/width..1000/horse-mock-up-laidout.png)

![](http://osp.kitchen/visual/osp.work.balsamine.2014-2015/495bd12e79b31c99fd789afc96dbb1eba9a1290e/width..1000/hatch-the-elephant-02.png)

![](http://osp.kitchen/visual/osp.work.balsamine.2014-2015/02bf874fac3b46a6689380a40d2e023bba355542/width..1000/hatch-the-elephant-01.png)

![](http://osp.kitchen/visual/osp.work.balsamine.2014-2015/a7133f02c1777042096eedc3175b82d09737e5e5/width..1000/paperjs-rice-ants.png)

![](http://osp.kitchen/visual/osp.work.balsamine.2014-2015/8724849bcc25774b7cc9fa2f2ff939938eff9a14/width..1000/numbering-in-javascript.png)

![](http://osp.kitchen/visual/osp.work.balsamine.2014-2015/cf5c343d591189db43d0b36b5d13ebf52e808054/width..1000/wall-programme-painted-by-Marc-De-Meyer.jpg)

## 1.

![](http://ospublish.constantvzw.org/images/var/albums/OSP-WTC/50350010.JPG) We are open source publishing, collective of graphic designers based in Brussels using only free and open-source software. We are interested in exploring and experimenting with our tools and the way we work. Open-source challenges you to question your software and to develop it yourself.

## 2.

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040639rr.jpg) ![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040640rr.jpg) ![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040641rr.jpg) ![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040642rr.jpg)

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040643rr.jpg)

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040643rr.jpg)

Open-source software is software that allows you to read it's source code and thus understand how it works. Free / Libre Open Source Software allows its user also to modify it and share that with others.

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.002.jpg)

Most design studio only show the output of the process and not the process itself. We are interested in showing the making, and to not only adresse a pair audience. In order to show the recipies and connect with a broader audience than just our pairs, we started to do print parties. We perform step by step and in parralel a graphic design and food recipies.

## 2 bis

![](http://gitlab.constantvzw.org/osp/workshop.typojanchi-seoul/raw/master/jam-gochujang-4freedoms-and-2restrictions/slides%20in%20hangul/1-use.png)

![](http://gitlab.constantvzw.org/osp/workshop.typojanchi-seoul/raw/master/jam-gochujang-4freedoms-and-2restrictions/slides%20in%20hangul/2-study.png)

![](http://gitlab.constantvzw.org/osp/workshop.typojanchi-seoul/raw/master/jam-gochujang-4freedoms-and-2restrictions/slides%20in%20hangul/3-modify.png)

![](http://gitlab.constantvzw.org/osp/workshop.typojanchi-seoul/raw/master/jam-gochujang-4freedoms-and-2restrictions/slides%20in%20hangul/4-share.png)

![](http://gitlab.constantvzw.org/osp/workshop.typojanchi-seoul/raw/master/jam-gochujang-4freedoms-and-2restrictions/slides%20in%20hangul/5-copyright.png)

![](http://gitlab.constantvzw.org/osp/workshop.typojanchi-seoul/raw/master/jam-gochujang-4freedoms-and-2restrictions/slides%20in%20hangul/6-copyleft.png)

FLOSS allows for many tools to co-exists. Designers using FLOSS don't have to use one-fits-all monolitic tool for their practices.

## 2 ter

![](http://strabic.fr/IMG/gif/fork-me-1413985803-1415567940.gif)

FLOSS encourages reappropriation and derivative while preserving the ownership credits of the different versions.

## 3.

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.005.jpg) Graphic design working with floss, started as an experiment at first with open-source equivalents of tools in a traditional workflow. More and more experimenting with tools not initally designed to make graphic design

## 4.

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/out-1_762-1327.gif)

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/laidout-grafik-tools.png)

As using these alternative tools show the patterns & behaviours we've teached ourselves

## 5.

![](http://osp.kitchen/api/osp.foundry.belgica-belgika/c23cefcf20b9b9117b160be71602491b20c74e83/blob-data/shapes-variation.png) Because the tool is open-source we can study it, understand, open and modify

## 6.

![](http://ospublish.constantvzw.org/images/var/albums/LGM-2013/DSCF3742_s.jpg?m=1396269176) But next to interacting with the tool itself if becomes possible to interact with the makers of our tools, by going to LGM.

## 7.

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.004.jpg) In these ways we can have a more intimate relationship with the tools we use

## 8.

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.010.jpg) More and more we started to experiment with technology by developing our own tools.

## 9.

![](https://osp.clouddiskspace.nl/index.php/s/fnXuTbdvfKvEMwR/download) The most exciting: html, because we can benefit from the vitality of webtechnologies but also it's an interesting hybrid between a canvas based design & programming.

## 10.

![](http://gitlab.constantvzw.org/osp/work.balsamine.2014-2015/raw/master/iceberg/numbering-in-javascript.png) Experiments with html solidified in html2print which we developed further in projects. It's the way we do research.

## 11.

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.016.jpg) Step by step we discover, understand, relearn the technologies we use. We even started to develop our own browser.

## 12.

![](http://ospublish.constantvzw.org/images/var/albums/LGM-Toronto-workshops/IMG_5073.JPG?m=1430257868) // Next to questioning the tools we use, we experiment with the way we work

## 13.

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.011.jpg) We used for example a collaborative editor and html2print to make graphic design

## 14.

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.023.jpg) In experimenting with tools & technologies we also try to exchange knowledge

## 15.

// Screen shot of collab agreement With every we client we are looking for a collaboration & exchange. For this we've written a collaboration agreement

## 16.

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.029.jpg) Sharing of knowledge through our site where we publish our sources, projects & process

## 17.

![](http://ospublish.constantvzw.org/images/var/albums/Up-Pen-Down/IMAG0188-2015142285.jpg?m=1396268099) Sharing through workshops with students

## 18.

![](http://ospublish.constantvzw.org/images/var/albums/Bootstrap-IV/DSCF2710_small.jpg?m=1396268334) Or sharing in a performative fashion: print party. Were we have to publicly share a part of our practice.

## 19.

![](http://ospublish.constantvzw.org/images/var/albums/Relearn-2013/relearn_small_1135.jpg?m=1396268944) Or in our summerschool re-learn: a summerschool based on the fast switching roles of teacher and student in open source.

## 20.

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.001.jpg) Using F/LOSS software because it pushes us, allows for other opportunities. Practice shapes tools shapes practice.

## L'OpenCon est LA conférence internationale de référence sur les questions d'Open Access, Open Data et Open Education (Open Science d'une manière plus générale).

## Open Access, Open Data et Open Education

## Open-source challenges you to question your software and to develop it yourself.

## education sharing knowledge

## Relearn

## Experiment in ways of working together

## Analogy of the moiré: used to one type of software, working with another tool can reveal the pattern in the one.

## Explanation to others in Médor

## Flyer for Balsamine using a collaborative editor

## Narrative: use html2print to benefit from web techno with a lot of info on it

## education amongst ourselves

## exchanges among partners

## with students/workshop

## describe different workshops

## Interesting aspect of Open-Source where you share back the tools you've where you are sometimes in the position of a teacher, sometimes in the postition of a student.

## It's one the questions we've discussed in our summerschool: Relearn.

## Analogy of the Moiré effect. When using other softwares you start to understand the particularities of your own software. As the moiré that reveals when imposing to pattern
