Title: FH Potsdam
Date: 2017-09-13
Translation: true
Lang: fr
Tags:

## FH Posdam

![](http://git.constantvzw.org/?p=osp.work.stories.git;a=blob_plain;f=wtc/wtc-postcard.jpg;h=03a45d19c6a889caf65cc571c8e0db5570e718f6;hb=HEAD)

![](http://ospublish.constantvzw.org/images/var/albums/OSP-WTC/50350008.JPG?m=1442311169)

<iframe src="http://ospublish.constantvzw.org/blog/news/is-it-possible" style="max-width:100%"></iframe>

![](http://git.constantvzw.org/?p=osp.workshop.saison-graphique.git;a=blob_plain;f=iceberg/cover-low.png;hb=HEAD)

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.014.jpg)

![](http://www.ludi.be/Bremen/balsa_2013-2014_44-45.jpg)

![](http://git.constantvzw.org/?p=osp.work.balsamine.2016-2017.git;a=blob_plain;f=iceberg/DSCF6143.jpg;hb=HEAD)

![](https://medor.coop/nuage/index.php/s/mJEYxarChAthj8J/download)

![](https://medor.coop/nuage/index.php/s/1SmpHskCnmoiFlw/download)

![](http://ospublish.constantvzw.org/images/var/albums/esadse.elif---workshop/IMG_20160405_180656.jpg?m=1460124971)

![](http://ospublish.constantvzw.org/images/var/albums/HFK-Bremen/DSCF3911.jpg?m=1453485823)

## OSP asbl / vzw

![](http://www.ludi.be/osp-statuts-fr-art3.png)

![](http://www.ludi.be/Bremen/collaboration-agreement-extract.jpg)

## Stroke fonts

## Belgica - Belgika

![](http://git.constantvzw.org/?p=osp.foundry.belgica-belgika.git;a=blob_plain;f=documentation/sticker-on-mac-1600px.jpg;h=d37a4230e2158297774001071273868e46bcdf54;hb=HEAD)

![](http://osp.kitchen/api/osp.foundry.belgica-belgika/763ed6c53987fe0ecec988282e7bdf5ed9bfa6de/blob-data/belgika-4-sample-fontforge.png) ![](http://ospublish.constantvzw.org/foundry/wp-content/uploads/Screen-shot-2015-03-20-at-14.34.51.png)

![](http://git.constantvzw.org/?p=osp.foundry.belgica-belgika.git;a=blob_plain;f=iceberg/venice-intro-letters.jpg;hb=HEAD)

![](http://git.constantvzw.org/?p=osp.foundry.belgica-belgika.git;a=blob_plain;f=iceberg/venice-lettering-pencil.jpg;hb=HEAD)

## FONS

![](http://ospublish.constantvzw.org/foundry/wp-content/uploads/univers_else_023.png)

![](http://www.ludi.be/Screenshot%20from%202016-06-23%2012%3A15%3A27.png)

## Strokify

![](http://www.ludi.be/Screenshot%20from%202016-06-23%2011%3A51%3A19.png)

## Metadin

![](http://git.constantvzw.org/?p=osp.foundry.belgica-belgika.git;a=blob_plain;f=documentation/metadin-tests.png;h=fb240f2b79a99cbf14c1903f6e8521f32ef27ed0;hb=HEAD)

## Franquin

![](http://www.ludi.be/Carnet-de-recherche-nov2015-lettre-R.png)

![](http://www.ludi.be/metafranquin-compil-screenshots.png)

![](http://www.ludi.be/module-01-notes-mi.jpg)

## Meta Hershey

![](http://git.constantvzw.org/?p=osp.foundry.belgica-belgika.git;a=blob_plain;f=documentation/plotter.jpg;h=552ece86018bf13a69ea141d4c40d440174e4e6e;hb=HEAD)

![](http://git.constantvzw.org/?p=osp.foundry.belgica-belgika.git;a=blob_plain;f=documentation/meta-hershey-relearn.png;h=8671ab2a0734307c7d7409538a4c1f10c767be6e;hb=HEAD)

## Html2print

## la Balsamine

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.011.jpg)

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.013.jpg)

![](http://git.constantvzw.org/?p=osp.work.stories.git;a=blob_plain;f=Theatre_de_la_Balsamine/balsa_2015-2016_4.jpg;h=9383734006f0fd13b995656017819cbf7929d5a4;hb=c534bd0ea70859980bc9344ca1d252c7338a4c14)

## Médor

![](http://www.ludi.be/Screenshot%20from%202016-06-23%2011%3A27%3A28.png)

![](http://www.ludi.be/Screenshot%20from%202016-06-23%2011%3A28%3A52.png)

![](https://medor.coop/nuage/index.php/s/CHbTcJ6s8rEg4zc/download)

## Bremen

![](http://www.ludi.be/Bremen/DSCF3993.jpg)

![](http://www.ludi.be/Bremen/DSCF4003.jpg)

## Esad Elif

![](http://ospublish.constantvzw.org/images/var/albums/esadse.elif---workshop/DSCF4919.JPG?m=1460124805)

![](http://ospublish.constantvzw.org/images/var/albums/esadse.elif---workshop/DSCF4910.JPG?m=1460124812)

## OSPkit

![](http://www.ludi.be/Capture%20d%27%C3%A9cran%20de%202016-06-23%2011-46-04.png)

## OSP sources

![](http://www.ludi.be/Screenshot%20from%202016-06-23%2010%3A54%3A40.png)

![](http://www.ludi.be/Screenshot%20from%202016-06-23%2010%3A55%3A45.png)
