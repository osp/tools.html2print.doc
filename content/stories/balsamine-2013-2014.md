Title: Balsamine 2013-2014
Date: 2017-09-13
Translation: true
Lang: fr
Tags: balsamine

## Making of

## Mode lyrique

Répondre à et infuser la programmation spéculative de la Balsa par un déboitement un peu perché au-dessus des sillons tracés par Gutenberg, il y a 550 ans quand il s'agit de composer typographiquement les pages d'un livre. OSP tente une composition des textes et des images en utilisant les langages neufs qui sont en train de transformer le web mois après mois. Les propositions jeunes et encore hésitantes, sur leurs pattes de jeunes poulains de ces langues qui pensent le web à venir, élargissent brutalement la manière dont des mots, des phrases et des visuels cohabitent comme des blocs de glace sur une rivière à la fonte des neiges. La notion de page est soudain nettement plus flottante et intervient à la fin du processus comme une scansion temporaire, comme une résille au rectangle pointe d'autres potentiels. Un document du temps qui coule.

![](../lgm-talk/30a.png) ![](../lgm-talk/30a.png)

fig 1 — figures extraites d'un lightning talk au Libre Graphics Meeting à Madrid, 12 avril 2013

## Mode technique

Depuis deux ans et demi, de multiples logiciels ont été utilisés pour la production graphique de la communication de la Balsa (fig. 1). Cette saison, OSP a décidé de faire le mur et de sauter dans le verger grouillant de l'HTML récent et des CSS plus récents encore (fig. 2). L'un et l'autre sont sortis de leur contexte naturel du web pour venir s'aventurer à produire les pages du présent petit livret. La liste des fonctionnalités nécessaires et des solutions qui peuvent y répondre se garnit (même si elle reste un peu moto-cross - fig. 3).

Concrètement, en gros, les 48 pages de ce programme sont concentrées dans une longue et grande page web (html + css). Un javascript dessine les marques de repérages nécessaires aux imprimeurs, page par page. Cette page est imprimée vers des pdf en fonction de leurs couleurs, séparées.

![](../lgm-talk/50a.png) ![](../lgm-talk/50a.png)

fig 2.

L'écologie HTML ↔ CSS ↔ navigateur ↔ système de fichier ↔ ligne de commande révèle la symbiose très articulée qui anime ces acteurs baignés par une culture commune et rend relativement fluide le flot de processus nécessaires à l'élaboration de ce type de publication. Le principal écueil encore présent, pour peu de temps, est la faible priorité actuelle donnée par le moteur libre Webkit aux routines permettant de découper le flux en pages. Un artisanat de détail à cette phase est donc encore nécessaire pour quelques mois.

Le fichier de la mise en page de la Balsa peut être visitée à l'aide de patience et d'un navigateur qui utilise un Webkit le plus récent possible, comme Chromium, et en activant les Webkit Experimental Features dans la page <span style="font-family: 'Ume P Gothic Balsa'; font-size: 12px; letter-spacing: 1px;">chrome://flags/</span> - [http://osp.constantvzw.org/work/balsamine.2013-2014/tree/master/programme/programme.html](http://osp.constantvzw.org/work/balsamine.2013-2014/tree/master/programme/programme.html)

![](../lgm-talk/55a.png) ![](../lgm-talk/55a.png)

fig 3.

Les fichiers de ce programme, et une vision méritocratique de leur genèse est à lire sur [http://osp.constantvzw.org/work/balsamine.2013-2014](http://osp.constantvzw.org/work/balsamine.2013-2014/) où un pourrait traduire la tétière par "Pour chacun de nos projets de cuisine et de graphisme, nous empilons des fichiers dans un dossier partagé. Le moment de chacun de ces empilements correspond à l'écriture de messages qui signent littéralement ces actes de —heu— commettre. Nous vous invitons chaleureusement à y goûter, à l'améliorer, ou à utiliser chacun de ces éléments comme ça vous semble bon ou mal et de les redistribuer à votre tour. Les seules formules de politesse que nous vous proposons sont de créditer proprement l'origine de ces mets et de les redistribuer sous un mode proche du «ne rien ôter à personne»", disons.
