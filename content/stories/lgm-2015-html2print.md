Title: LGM 2015
Date: 2017-09-13
Translation: true
Lang: fr
Tags:

## html2print

## html2print

## HTML for print

Working on the web has made us used to a certain way of working, dynamic and in collaboration, that no traditional layout program offered—neither proprietary nor free.

The departure away from canvas based layout has many reasons (frustrations) but the benifits are multiple. It's within the very fabric of the web that we find great potential for publishing layout.

## Reasons to make the jump

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.016.jpg)

## making the jump

*   exciting CSS specifications for print
*   increasing development of online typography
*   accessible languages: many resources

## Benefits of using HTML/browser for print

## Collaboration (etherpad, git)

![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/relearn-ethertoff-write.png)

## More benefits enabled by the web

*   Mix between code design and visual design
*   Being able to inspect rendered items
*   Scripting with Javascript

## Drawbacks of using web browsers for print

*   dependant on browsers to implement missing CSS features
*   RGB by default (we convert it with Ghostscript)
*   spot colours difficult
*   Browsers pdf rendering is a black box
*   Browsers unstable platforms

## Examples of earlier projects

## Balsamine 2013—2014

## Balsamine 2013—2014

### Mix of spot colours made possible with Less CSS

![Mixed inks screenshot](http://osp.kitchen/api/osp.tools.html2print/raw/iceberg/mixed-inks.png)

## Balsamine 2013—2014

### Usage of "float" elements, inherent to HTML & CSS

### Cut off and feed through of images to visualize the continuous flow of the document

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.013.jpg)

## Balsamine 2013—2014

### Use of HTML tables

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.014.jpg)

## Balsamine 2013—2014

### Use of CSS regions to layout the snail of text

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.015.jpg)

## Balsamine 2014—2015

## Balsamine 2014—2015

### View as spread in the browser

![](http://osp.constantvzw.org/api/osp.work.balsamine.2014-2015/396dff06bd594696a92ea3de6511e9bf8f158161/blob-data/view-as-spread-in-browser.png)

## Balsamine 2014—2015

### Automatic numbering of the figures

![](http://osp.kitchen/api/osp.work.balsamine.2014-2015/raw/iceberg/numbering-in-javascript.png)

## Balsamine 2014—2015

### Visual design within the browser for posters

<figure>![](http://osp.kitchen/api/osp.tools.html2print/cfde7309853877889c9239bef3aa448418f17434/blob-data/season-poster-in-html.png)

<figcaption>

View in the browser

</figcaption>

</figure>

<figure>![](http://osp.kitchen/api/osp.tools.html2print/3d8ca125f7ab39efe0a7e18e2a518000ffa75721/blob-data/season-poster-offset-export.png)

<figcaption>

PDF export for offset, SVG colors changed directly in the browser

</figcaption>

</figure>

## V/J 14

Made with Ethertoff, a wiki based on Etherpad.

## V/J 14

### Generated cover with tags

![](http://osp.kitchen/visual/osp.work.vj14/8973e6e61c052ad3c22f26c984678e0836f42c8c/width..600/DSC_9959.png.png)

## V/J 14

### Spread showing how images come out of the text block area

![](http://osp.kitchen/visual/osp.work.vj14/dde6897be0ffb59d60827448999c1e7713e31bba/width..600/DSC_9957.png.png)

## Balsamine 2015—2016

*   http://localhost:8001
*   http://osp.constantvzw.org:9999/p/balsa2015-2016-html
*   http://osp.constantvzw.org:9999/p/balsa2015-2016-css

## Balsamine 2015—2016

### → DEMO ←

## Workshop @ HEAR Strasbourg

## Workshop @ HEAR Strasbourg

*   As an example of collaboration
*   Experimenting what would be media queries for print.
*   What styles are shared? What styles are specific to a format?
*   Feel how flow design is different from visual design

## Responsive print?

![Imgur](http://i.imgur.com/yuB33Yv.jpg)

### → DEMO ←

http://localhost:8002

## Anatomy of the repository

### → DEMO ←

## Prototypes

Can be done manually for now, need to be implemented with scripts or buttons.

## Flat plan

![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/vj14_chemin-de-fer-in-browser.png)

## Imposition

## Moveable items

## Content from existing webpages

## And now?

*   Choose a license
*   Facilitate the input of content

## Which webkit browser?

*   **Chromium** → after version 33 css regions don't work
*   Epiphany/Web → format ok, awful web inspector
*   **Konqueror** → css regions don't work
*   Midori → format ok, awful web inspector as Epiphany, a bit slow and unstable (crashes)
*   **Opera** → css regions don't work,
*   **Qtweb** → format doesn't work
*   **QupZilla** → can't make custom format or format not ok
*   **rekonq** → can't make custom format or format not ok
*   Safari → format ok, mac only
*   **Vivaldi** → css regions don't work, but quite interesting design-wise !
*   **otter browser** → can't make custom format
*   arora → web inspector ok, light and stable, format not ok
*   **netrider** → awfully long to compile from package manager, ugly and regions don't work
*   **vimb** → can't change page format, but kiosk mode available and vim-like bindings
*   **wildfox** → cannot print

## Use and contribute to our tool:

*   [http://osp.kitchen/tools/html2print](http://osp.kitchen/tools/html2print)
*   [https://github.com/osp/osp.tools.html2print](https://github.com/osp/osp.tools.html2print)
