Title: Chaumont 2017
Date: 2017-09-13
Translation: true
Lang: fr
Tags:

## workshop-chaumont-2017.md

Alors que la matérialité de l'informatique devient de plus en plus difficile face à l'apparente fluidité des interfaces «intuitives», Open Source Publishing continue d'interroger le rôle des outils numériques dans les processus créatif. OSP a expérimenté différents outils libres de mise en page, dont on peut dire qu'ils se divisent grossièrement en deux catégories: d'un côté une approche essentiellement visuelle, et de l'autre une approche intégralement programmatique.

Dans sa tentative de simuler la manipulation directe de l'objet final, l'approche WYSIWYG* se heurte aux limites du paradigme papier/ciseaux. Prisonnière de l'héritage de Gutenberg, elle ignore le potentiel de réinvention du média numérique. L'approche programmatique s'avère elle ausi décevante car systématique et linéaire. Fonctionnant à sens unique, du code vers le visuel, elle fonctionne bien pour mettre en page des flux continus mais permet très difficilement de débrayer et de créer des mises en pages plus articulées, notamment car le format final n'est plus éditable.

Et puis il y a le web...

Ce qui nous séduit dans le design web est notamment qu'il ne demande pas de faire ce choix infernal entre design visuel et design par le code. Populaires, ultra-documentés et basés sur des formats ouverts élaborés par différents acteurs dans un souci de dialogue et de continuité, les langages du web sont de véritables lingua franca: ils sont éditables par de nombreuses manières, «à la main» et via un large spectre de logiciels visuels ou programmatiques. Le navigateur web, pièce maîtresse, réunit en un même espace ces différentes approches. Par sa nature distribuée (une page étant généralement le résultat de l'aggrégation de nombreuses ressources telles des feuilles de style ou des images), les formats du web permettent un travail collaboratif entre des personnes aux compétences diverses, contrastant avec l'approche solitaire des logiciels comme InDesign ou Scribus.

Ne pourrions-nous pas utiliser les mêmes méthodologies et outils pour le design imprimé?

Design génératif, mises en pages dynamiques et flux de production délinéarisés... Lors de ce workshop nous nous intéresserons aux nouvelles possibilités offertes au design imprimé par cet écosystème singulier que forment les technologies du web.

### A demander

_-Démander les conditions de l'internet, à savoir si il faut ramener un router._ - est-ce les ordis de l'école? Est-ce sur mac? _- install party? Machine virtuelle_ - Quels outils de manière générale * 4 jours

ecriture expressive. Cantatrice chauve. En HTML sous titre ou théatre script au sens large

groupes de 2-3 tester des mises en pages en pages audacieuse

contraintes. ex: 100 lignes de styles, écrire du code css sous contrainte, oulipo

La cantatrice Chauve - massin http://idata.over-blog.com/2/31/51/01/767/Massin_Ionseco_18.jpg

il pleut appolinaire: https://jsfiddle.net/jjs6qfpa/ epicpedia lewis caroll: http://teibyexample.org/examples/TBED04v00.htm?target=carroll

http://2.bp.blogspot.com/-OHJqEjUfFtI/T1ld52c6W5I/AAAAAAAAA-U/1lAHf0Vsq8U/s640/MOUSE%27S+TALE.jpg

dictionnaire de bayle

la maison des feuilles https://www.images-booknode.com/book_cover/3789/full/la-maison-des-feuilles-3789253.jpg

http://www.antoniomiranda.com.br/poesia_visual/img/concrete_poetry_looking.jpg

Contrainte: - Séquence (flux rss, sous-titre, log, etc). Pas un dialogue? (ex texte de loi oui mais dialogue de théâtre non) - le rendre expressif, en utilisant l'espace comme moyen expressif - jouer avec la structure, ecrite par des règles, reproductible sur d'autre contenu - contenu libre mais si rien trouvé dans les xxx minutes, on vous assigne un truc - Comment documentent-ils. Présentation HTML2print - quels projets montrer? Future? Médor? etc. Quel objets on ammène?

Quels savoirs on t-il besoin? comment trouver des fontes command line -> http://aan.stdin.fr/10_choses_%C3%A0_faire_avec_la_ligne_de_commande expressions rationelles -> http://aan.stdin.fr/Expressions_rationnelles

### Missions

*   écrire mail à Luc _- présentation du ws sommaire_ - son numéro de téléphone _- réservation hotêl_ - questions techniques -> Alex *
*   écrire l'énoncé (presentation HTML2print, enjeux, contraires) (antoine & Alex)

*   sélectionner les exemples HTML2print (d'abord les listers)

*   recolter des références pour le workshop (continuer la liste)

*   créer une série de démos boite à outil des fonctionnalités HTML/CSS qu'on imagine
*   réfléchir les mini-cours qu'on veut donner _- les propriétés css de déformation skew, scale, rotate,_ -
