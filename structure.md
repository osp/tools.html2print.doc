Accueil
=======
Pourquoi ? Qu'est-ce que c'est ?
Comment en est-on arrivés là ?

Manuel
------
* installation, premier lancement, commandes etc. - **interne à html2print**
* some usefull command lines - **externe à html2print**

Showcase
--------
* montrer des projets faits avec html2print : photos objets imprimés, photos de workshops, textes etc. - **interne à html2print**
* montrer des projets similaires, ce qui se fait dans le domaine : projets, articles, posts, photos etc. - **externe à html2print**
