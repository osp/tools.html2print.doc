Title: Web Engines 2015
Date: 2017-09-13
Translation: true
Lang: fr
Tags:

## web-engines-2015.md

## Open Source Publishing, Brussels

### Design with Free and Open Source Software

#### Print publishing with Web browsers

![](http://i.liketightpants.net/and/assets/called/osp-frog.png)

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/osp-crop-wtc.jpg)

![](http://osp.kitchen/visual/osp.work.balsamine.2013-2014/52d8290a8dc8787662a0dd3d2b8ea85f4db8bac6/width..1000/IMG_20130514_192935.jpg)

![](http://life-after-the-template.ericschrijver.nl/images/vj-spread.jpg)

![](http://www.etienneozeray.fr/libre-blog-images/balsa-2014.JPG)

![](http://osp.kitchen/visual/osp.work.balsamine.2015-2016/2d466b2e7ed8f83fc2b44c7608db5862906b3c2d/width..1000/DSCF8651_01.jpg)

## Why publish printed documents with HTML?

*   the tools are Open Source and/or Free Software
*   there is a huge ecosystem (esp. in comparison to other FLOSS publishing tools)
*   plain text file formats enable collaboration (git, etherpad)
*   convergence between print and screen

## The good

*   Ctrl+p

## The bad

*   Specs are limited
*   Support even more so

## CSS 2.1

*   Blink has best support
*   Webkit and Gecko don’t seem to support @page margins, :left :right pseudo classes

[http://www.w3.org/TR/CSS2/page.html](http://www.w3.org/TR/CSS2/page.html)

    @page :left {
      margin-left: 4cm;
      margin-right: 3cm;
    }

    @page :right {
      margin-left: 3cm;
      margin-right: 4cm;
    }

## CSS3

*   Still in development, not supported by browsers
*   Page model feels limited—could it be more generic?
*   Generated content overengineered? Fear of JavaScript?

[http://www.w3.org/TR/css3-page/](http://www.w3.org/TR/css3-page/)  
[https://drafts.csswg.org/css-gcpm-3/](https://drafts.csswg.org/css-gcpm-3/)

    @page {
      margin: 4cm 3cm 2cm 3cm;

      @top-center {
        font-family: sans-serif;
        font-size: 2em;
        content: counter(page);
      }
    }

## HTML2PRINT

*   our workaround
*   css regions
*   [http://osp.kitchen/tools/html2print/](http://osp.kitchen/tools/html2print/)

![](http://silo.schr.fr/MedorHTML2PRINT.png)

![](http://silo.schr.fr/MedorHTML2PRINTexplanations.png)

Thank you: All organisers and attendees of Web Engines Hackfest 2015, Igalia and Collabora for sponsoring the Hackfest, and the Creative Industries Fund NL for sponsoring Eric Schrijver’s travel expenses.
