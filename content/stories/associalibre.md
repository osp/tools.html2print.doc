Title: Associalibre
Date: 2017-09-13
Translation: true
Lang: fr
Tags:

## Presentation Associalibre 2016

## Médor

![medor 2](https://medor.coop/nuage/index.php/s/lAiTeS7tJFsOoM5/download)

## 1 an et 4 numéros déjà

![4 numéros](https://medor.coop/nuage/index.php/s/vkBiIXRY756BEHc/download)

## requestionner le journalisme et la manière de produire des médias

![cover 1](https://medor.coop/nuage/index.php/s/14kZvGStxJLpbOw/download)

![cover 3](https://medor.coop/nuage/index.php/s/at6ycn2hcoj0rvd/download)

![cover 4](https://medor.coop/nuage/index.php/s/V92TzbsT3h0Hewe/download)

## Remettre à plat les manières de travailler

## Le collectif, l'équipe et les fondateurs

![l'equipe](https://medor.coop/nuage/index.php/s/yCLfnXBcNzI7wam/download)

## La coopérative

![l'AG](https://medor.coop/nuage/index.php/s/L9zAD2KkmSTH3JJ/download)

![](http://git.constantvzw.org/?p=osp.work.stories.git;a=blob_plain;f=wtc/wtc-postcard.jpg;h=03a45d19c6a889caf65cc571c8e0db5570e718f6;hb=HEAD)

![](http://ospublish.constantvzw.org/images/var/albums/OSP-WTC/50350008.JPG?m=1442311169)

<iframe src="http://ospublish.constantvzw.org/blog/news/is-it-possible"></iframe>

## HTML2print

Depuis 2006, nous expérimentons, au sein d'OSP, de nombreux logiciels libres ou open source de mise en page: Scribus, Context, Enscript, Laidout, Inkscape, LibreOffice… Certains proposent une approche WYSIWYG, permettant un retour visuel immédiat mais trop souvent limitée au paradigme papier/ciseaux; d'autres adoptent une approche programmatique, idéale pour traiter des flux mais se prêtant mal à des mises en pages plus articulées.

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.005.jpg)

Parallèlement, nous travaillons sur des projets web dont les formats HTML et CSS permettent, par leur nature (ouverts, textuels, structurés), une grande diversité d'outils et d'approches (textuelles, visuelles ou programmatiques), mais dont la portée se limitait jusque là au design pour écran. C'est pourquoi nous construisons depuis 2013 «html2print»: un ensemble d'outils et de recettes qui utilisent les technologies du web pour concevoir des mises en page imprimées.

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.010.jpg)

*   specification CSS pour l'imprimé
*   amélioration rapide des possibilités typographiques
*   ultra-documenté
*   déjà utilisé à différent niveaux
*   retro-compatible, soutenable
*   ouvert par nature
*   un écosystème de bibliothèques
*   permet une approche à la fois visuelle et programmatique
*   possibilités cross-media

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.016.jpg)

*

## Problèmes

*   dépendance aux navigateurs pour implémenter les fonctionanalités CSS nécessaires
*   sortie RGB, pas de ton directs
*   La sortie PDF des navigateurs semble être une boite noire impénetrable
*   Certaines des fonctionnalités sont instables

## Utilisation d'un etherpad pour le design

*   Design collectif en temps réel
*   Avoir une table des matières interactives pour naviguer dans le code ![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.011.jpg)

## Double-pages du programme

*   Utilisation d'éléments en "float" propre au HTML
*   Coupure des images en fin et début de page pour révéler le flux continu du contenu

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.013.jpg)

## Utilisation de tableau HTML

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.014.jpg)

## Colophon

Utilisation du chaînage de texte pour l'escargot

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.015.jpg)

![](https://medor.coop/cuisine/images/thumb/3/39/Bug-style.png/600px-Bug-style.png)

![](https://medor.coop/cuisine/images/d/db/Extrait-bug_approches.jpg)

<figure>![liste article](https://osp.clouddiskspace.nl/index.php/s/7yDm2bAfo5APWZq/download)

<figcaption>

Liste des articles. Des options permettent de filtrer les articles par numéro et statut.

</figcaption>

</figure>

<figure>![editeur de texte](https://osp.clouddiskspace.nl/index.php/s/aPtKWfe0zhkre1z/download)

<figcaption>

Éditeur d'articles. Les options de l'éditeur sont adaptées à la charte graphique de Médor.

</figcaption>

</figure>

<figure>![Sommaire](https://osp.clouddiskspace.nl/index.php/s/RwEKAP8VBeYnGtz/download)

<figcaption>

Gestion du sommaire des numéros. Cette interface permet de sélectionner et d'organiser le contenu d'un numéro. À posteriori, elle permet de publier en ligne le sommaire sur le site.

</figcaption>

</figure>

<figure>![JSON](https://osp.clouddiskspace.nl/index.php/s/raWBCfc7eAvC48l/download)

<figcaption>

Interface présentant le contenu des numéros sous forme structurées (API). Elle permet l'export et l'import programmatique de contenus.

</figcaption>

</figure>

<figure>![JSON2](https://osp.clouddiskspace.nl/index.php/s/d3LPxCGsUr8Ryrn/download)

<figcaption>

Détail de l'interface programmatique pour le numéro quatre.

</figcaption>

</figure>

<figure>![Gestionnaire d'images](https://osp.clouddiskspace.nl/index.php/s/ntfh6eZ3QWWylgT/download)

<figcaption>

Gestion des images sur le serveur medor.coop

</figcaption>

</figure>

## Plateforme de mise en page

<figure>![OSPKit](https://osp.clouddiskspace.nl/index.php/s/Ygw53slzn81NdCg/download)

<figcaption>

OSPKit: le navigateur assemblés par OSP pour un meilleurs support des fonctionnalités nécéssaires à la mise en page.

</figcaption>

</figure>

<figure>![OSPKit print](https://osp.clouddiskspace.nl/index.php/s/6B27A2CZCdy7kAF/download)

<figcaption>

Impression d'articles dans OSPKit

</figcaption>

</figure>

<figure>![OSPKit + inspector](https://osp.clouddiskspace.nl/index.php/s/fnXuTbdvfKvEMwR/download)

<figcaption>

Vue d'OSPKit avec l'inspecteur de code. Il permet d'ajuster les feuilles de styles et de voir le résultat instantanement.

</figcaption>

</figure>

<figure>![Bugreport webkit](https://osp.clouddiskspace.nl/index.php/s/jYGX30ziUAICsoA/download)

<figcaption>

Exemple de contribution au moteur de rendu d'OSPKit (rapport d'erreur)

</figcaption>

</figure>

![HTML5Lib Typogrify](https://osp.clouddiskspace.nl/index.php/s/w477EMlGeVjRzNA/download)

<figcaption>

Page d'accueil du projet d'enrichissement typographique créé pour Médor

</figcaption>

</figure>
