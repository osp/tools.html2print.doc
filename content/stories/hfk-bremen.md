Title: HFK Bremen
Date: 2017-09-13
Translation: true
Lang: fr
Tags:

## OSP Bremen

![](http://git.constantvzw.org/?p=osp.work.stories.git;a=blob_plain;f=wtc/wtc-postcard.jpg;h=03a45d19c6a889caf65cc571c8e0db5570e718f6;hb=HEAD)

![](http://ospublish.constantvzw.org/images/var/albums/OSP-WTC/50350008.JPG?m=1442311169)

<iframe src="http://ospublish.constantvzw.org/blog/news/is-it-possible"></iframe>

![](http://ospublish.constantvzw.org/images/var/albums/Print-Party-Hachures-Tourneurs/IMG_7297.JPG?m=1414097158)

![](http://ospublish.constantvzw.org/images/var/albums/Up-pen-down--01/2015-10-25%2017_34_20.jpg?m=1448905825)

![](http://www.ludi.be/Bremen/osp-statuts.jpg)

![](http://www.ludi.be/Bremen/collaboration-agreement-extract.jpg)

## Stroke fonts

![](http://osp.kitchen/api/osp.foundry.belgica-belgika/fb240f2b79a99cbf14c1903f6e8521f32ef27ed0/blob-data/metadin-tests.png)

![](http://git.constantvzw.org/?p=osp.foundry.belgica-belgika.git;a=blob_plain;f=documentation/plotter.jpg;h=552ece86018bf13a69ea141d4c40d440174e4e6e;hb=HEAD)

![](http://git.constantvzw.org/?p=osp.foundry.belgica-belgika.git;a=blob_plain;f=documentation/meta-hershey-relearn.png;h=8671ab2a0734307c7d7409538a4c1f10c767be6e;hb=HEAD)

![](http://git.constantvzw.org/?p=osp.foundry.belgica-belgika.git;a=blob_plain;f=documentation/sticker-on-mac-1600px.jpg;h=d37a4230e2158297774001071273868e46bcdf54;hb=HEAD)

![](http://osp.kitchen/api/osp.foundry.belgica-belgika/763ed6c53987fe0ecec988282e7bdf5ed9bfa6de/blob-data/belgika-4-sample-fontforge.png) ![](http://ospublish.constantvzw.org/foundry/wp-content/uploads/Screen-shot-2015-03-20-at-14.34.51.png)

![](http://git.constantvzw.org/?p=osp.foundry.belgica-belgika.git;a=blob_plain;f=iceberg/venice-intro-letters.jpg;hb=HEAD)

## Théâtre la Balsamine

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.009.jpg)

![](http://osp.constantvzw.org/api/osp.work.balsamine.2011-2012/ff763721b60c9b55c3ddb708cd96cfeff004b058/blob-data/fanzine-wip.png)

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.011.jpg)

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.013.jpg)

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.014.jpg)

![](http://www.ludi.be/Bremen/balsa_2013-2014_44-45.jpg)

![](http://osp.constantvzw.org/api/osp.work.balsamine.2014-2015/396dff06bd594696a92ea3de6511e9bf8f158161/blob-data/view-as-spread-in-browser.png)

![](http://osp.kitchen/api/osp.work.balsamine.2014-2015/raw/iceberg/numbering-in-javascript.png)

![](http://git.constantvzw.org/?p=osp.work.stories.git;a=blob_plain;f=Theatre_de_la_Balsamine/balsa_2015-2016_1.jpg;h=350bdd75427ba061dc2fdb11f1e41d94e944daa4;hb=c534bd0ea70859980bc9344ca1d252c7338a4c14)

![](http://git.constantvzw.org/?p=osp.work.stories.git;a=blob_plain;f=Theatre_de_la_Balsamine/balsa_2015-2016_2.jpg;h=685306c53045b02a099653fbc3504be61ac2f8a6;hb=c534bd0ea70859980bc9344ca1d252c7338a4c14)

![](http://git.constantvzw.org/?p=osp.work.stories.git;a=blob_plain;f=Theatre_de_la_Balsamine/balsa_2015-2016_4.jpg;h=9383734006f0fd13b995656017819cbf7929d5a4;hb=c534bd0ea70859980bc9344ca1d252c7338a4c14)

![](http://git.constantvzw.org/?p=osp.work.stories.git;a=blob_plain;f=Theatre_de_la_Balsamine/balsa_2015-2016_5.jpg;h=d0045375d661bc2fad8916dc2fa6323dc0aa901a;hb=c534bd0ea70859980bc9344ca1d252c7338a4c14)

![](http://www.ludi.be/Bremen/DSCF3925.jpg)

## f-u-t-u-r-e.org

![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/future_write.png)

![](http://osp.kitchen/api/osp.tools.ethertoff/e5ed65a8c42fdc11643eb997b11cd804ed047466/blob-data/future_automatic-index.png)

![](http://osp.kitchen/api/osp.tools.ethertoff/18b9bf96004b263882e8d8637ca7f7313db1265b/blob-data/future_screen.png)

![](http://osp.constantvzw.org/api/osp.tools.ethertoff/3e03d472b36269cd0fc989dad1cf792c0438e279/blob-data/future_laser.png)

![](http://osp.constantvzw.org/api/osp.tools.ethertoff/c6153a1d69859b2490fc94f468a5ad35f806d55d/blob-data/future_offset.png)

## Médor

![](http://osp.kitchen/api/osp.work.stories/4d4d0694acdaa0513416e5133b963f83fdfae055/blob-data/medor-1-reprint-december-2015.png)

![](http://www.ludi.be/Bremen/medor1-cover-full.jpg)

![](http://www.ludi.be/Bremen/medor1-sommaire.jpg)

![](http://www.ludi.be/Bremen/medor1-94-95.jpg)

![](http://osp.kitchen/api/osp.work.stories/be670a0d7c4b3376e938bca52b645cfc5cd75470/blob-data/medor1-10-11.jpg)

## Workshop - HEAR Strasbourg

![](http://comgraph.hear.fr/wp-content/uploads/2015/03/capture-dcran-2015-03-13--11-17-00.png)

![](http://i.imgur.com/yuB33Yv.jpg)

## Workshop - Hfk Bremen

![](http://www.ludi.be/Bremen/DSCF3909.jpg)

![](http://www.ludi.be/Bremen/DSCF3911.jpg)

![](http://www.ludi.be/Bremen/DSCF3914.jpg)
