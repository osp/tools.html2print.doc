Title: My First Review
Date: 2010-12-03 10:20

## SOME USEFUL COMMAND LINES TO ALTER PDFS.

### SPLIT PDFS

```bash
gs -dBATCH -sOutputFile="test.pdf" -dFirstPage=3 -dLastPage=3 -sDEVICE=pdfwrite output.pdf
```

### COMBINE PDFS

```bash
gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE=output.pdf  -dBATCH file1.pdf file2.pdf file-x.pdf
```

### CLEANING PDF (LOWERS FILE SIZE WITHOUT LOSING QUALITY)

```bash
gs -dNOPAUSE -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOUTPUTFILE=output.pdf  -dNOPAUSE -dBATCH  input.pdf
```

### LOWER FILE SIZE

```bash
# the value "/ebook" compress bitmaps but leaves vector graphics and text intact
# 72 is for 72dpi, change at your will
gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE=output.pdf  -dBATCH -dPDFSETTINGS=/ebook  -dColorImageDownsampleType=/Bicubic  -dColorImageResolution=72 -dGrayImageDownsampleType=/Bicubic -dGrayImageResolution=72  -dMonoImageDownsampleType=/Bicubic   -dMonoImageResolution=72  input.pdf
```

### CROP

```bash
# The origin (0, 0) of the coordinates in PDF in the bottom left corner of the page.
# (19, 34)  are the coordinates of the bottom-left corner of the output area.
# (438.5 600.5) are the coordinates of the upper-left corner of the output area.
gs -o output.pdf -sDEVICE=pdfwrite -c "[/CropBox [19 34 438.5 600.5] /PAGES pdfmark" -f input.pdf
```

### SHIFT PAGE

Save this command in a file shift.ps:

<</PageOffset [-100 0]>> setpagedevice

Then concatenate this file with your PDF to apply it:

```bash
gs -dNOPAUSE -sDEVICE=pdfwrite -dNOPAUSE -dBATCH  -sOUTPUTFILE=output.pdf shift.ps input.pdf
```

### TRANSFORMS IN BLACK AND WHITE

```bash
gs -sOutputFile=grayscale.pdf -sDEVICE=pdfwrite -sColorConversionStrategy=Gray -dProcessColorModel=/DeviceGray -dCompatibilityLevel=1.4 -dNOPAUSE -dBATCH color.pdf
```

### CMYK CONVERSION

```bash
gs -dNOPAUSE -sDEVICE=pdfwrite -dGraphicKPreserve=2 -sColorConversionStrategy=CMYK -dProcessColorModel=/DeviceCMYK -dNOPAUSE -dBATCH  -sOUTPUTFILE=CMYK.pdf  RGB.pdf
```
