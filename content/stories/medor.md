Title: Medor
Date: 2017-09-13
Translation: true
Lang: fr
Tags:

## Médor

## Les outils de Médor

## Parler logiciel à une AG Médor?

*   Comme pour tout projet, Médor se pense, se construit, s'écrit, se dessine, s'imprime, se promeut, se distribue avec des logiciels.
*   Le logiciel n'est ni mal ennuyeux mais nécessaire, ni envoûtement.
*   Les logiciels peuvent être des outils à penser ![](http://ospublish.constantvzw.org/images/var/resizes/Relearn-2013/relearn_small_0960.jpg?m=1396267891)

## Et logiciel libre?

*   Le logiciel peut résister et questionner
*   Sa rugosité met en lumière
*   Ses contraintes viennent avec une liberté qui secoue nos esthétiques
*   Son fonctionnement est intrinsèquement collectif
*   Il est culture libre, les yeux ouverts ![](http://ospublish.constantvzw.org/images/var/resizes/Relearn-2013/relearn_small_1135.jpg?m=1396267900)

## OK, mais comment ça marche?

## Liberté 0 : utiliser

<figure>![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040639rr.jpg)

<figcaption>

The freedom to run the program as you wish, for any purpose

</figcaption>

</figure>

## Liberté 1 : ouvrir et regarder

<figure>![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040640rr.jpg)

<figcaption>

The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1). Access to the source code is a precondition for this.

</figcaption>

</figure>

## Liberté 2 : partager

<figure>![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040641rr.jpg)

<figcaption>

The freedom to redistribute copies so you can help your neighbor (freedom 2)

</figcaption>

</figure>

## Liberté 3 : modifier et distribuer

<figure>![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040642rr.jpg)

<figcaption>

The freedom to distribute copies of your modified versions to others (freedom 3). By doing this you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.

</figcaption>

</figure>

## Contrainte 0 : attribuer

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040643rr.jpg)

## Contrainte 1 : ne pas refermer

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040644rr.jpg)

## Deux licences

*   Logiciel et graphisme libre
*   Textes libres à diffuser, pas modifier

## Le HTML comme lingua franca

Un environnement permettant d'ouvrir les processus éditoriaux.

*   L'HTML est un langage très pérenne et super documenté
*   C'est un format très souple, qui va nous permettre de construire des mises en page transposables d'une surface à une autre : site web, tablette, ebook, pdf
*   Une page est composée de multiple ressources sur lesquelles nous pouvons travailler à plusieurs
*   C'est un format texte donc très facile à versionner
*   Une pratique en ébullition.

## Adobe Regions

![](https://medor.coop/cuisine/images/e/ee/Article_regions_reflow.png)

## html2print

→ http://osp.kitchen/tools/

## La Balsamine 2013 - 2014

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.013.jpg)

## La Balsamine 2013 - 2014

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.014.jpg)

## La Balsamine 2014 - 2015

![](http://www.ludi.be/balsa-programme_2014-2015.png)

## La Balsamine 2014 - 2015

![](http://www.ludi.be/basa_involution_iv_2.png)

## VJ14

![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/vj14_spread-in-browser.png)

## VJ14

<figure>![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/vj14_chemin-de-fer-in-browser.png)

<figcaption>

Chemin de fer dans le navigateur

</figcaption>

</figure>

## VJ14

![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/vj14_pdf.png)

## html2print & Médor

## «Le jour où la Belgique a buggé»: première enquête de Médor.

## Aperçu d'une double page

![](http://www.ludi.be/medor/bug_6-7.png)

![](http://www.ludi.be/medor/bug-flow.png)

## HTML

![](http://www.ludi.be/medor/bug-html.png)

## LESS/CSS

![](http://www.ludi.be/medor/bug-style.png)
