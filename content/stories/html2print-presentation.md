Title: html2print Présentation
Date: 2017-09-13
Translation: true
Lang: fr
Tags:

## html2print presentation

## Open Source Publishing

## [http://osp.kitchen/](http://osp.kitchen/)

Depuis 2006, Open Source Publishing pratique le design graphique avec des logiciels libres et open source.

Nous sommes aujourd'hui à Bruxelles, 8 femmes et hommes venant de différents domaines.

## Pourquoi le logiciel libre?

*   Pour pouvoir modifier des choses qui ne nous conviennent pas forcément dans nos outils quotidiens
*   Pour expérimenter de nouveaux outils et ne pas tomber dans une routine de confort
*   Pour utiliser des logiciels dans un autre but que celui d'origine
*   …

## Freedom 0

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040639rr.jpg)

## Freedom 1

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040640rr.jpg)

## Freedom 2

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040641rr.jpg)

## Freedom 3

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040642rr.jpg)

## Rule 0

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040643rr.jpg)

## Rule 1

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040644rr.jpg)

## Nous préférons dire que nous sommes une cuisine.

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.002.jpg)

## Nos recettes

![](osp.kitchen/tools/visualculture/tree/master/iceberg/css-2014.png)

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.030.jpg)

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.031.jpg)

## Print party

Pour partager notre pratique, nous organisons des _Print Parties_. Des événements publics où nous produisons en _live_ un objet graphique avec des logiciels libres et open source.

À côté des ordinateurs, nous cuisinons en même temps. Imaginez une recette de cuisine non-libre:

*   on ne pourrait pas demander les ingrédients (interdit par la loi!);
*   on ne pourrait pas la refaire;
*   on ne pourrait pas la partager;
*   on ne pourrait pas la modifier (même s'il manque un ingrédient).

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.003.jpg)

## Théâtre la Balsamine

## 2011–2012

## Wiki de la Balsamine

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.009.jpg)

Utilisation de PmWiki; un wiki fait pour être hyper malléable.

## Affiche Inkscape + traitement d'images Gimp

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.006.jpg)

## Affiche Inkscape inspiré par les diagrammes de Gantt

![](http://osp.constantvzw.org/api/osp.work.balsamine.2011-2012/3516df3f7b4abf5b9277d70f13177fe6f8fe4192/blob-data/balsa-affiche2012-print.jpg)

## Page simple sur ConTeXt

![](http://osp.constantvzw.org/api/osp.work.balsamine.2011-2012/043a5758072160d0c2d0298a5a7f7655ab2e41d5/blob-data/genese-jour4.png)

## Boîtes englobantes sur ConTeXt

*   bordures autour des mots impossibles à obtenir sur Scribus ou Indesign
*   de même pour le croisement des boîtes englobantes ![](http://osp.constantvzw.org/api/osp.work.balsamine.2011-2012/350e648de26f0feaab85517a217102556614cc95/blob-data/genese-page14-15.png)

## Mix ConTeXt + Scribus

![](http://stdin.fr/uploads/Works.Balsamine/arcas-back.png)

## LibreOffice Calc

![](http://osp.constantvzw.org/api/osp.work.balsamine.2011-2012/ff763721b60c9b55c3ddb708cd96cfeff004b058/blob-data/fanzine-wip.png)

## 2012–2013

## GraphViz + Inkscape

![](http://osp.constantvzw.org/api/osp.work.balsamine.2012-2013/1c5d4817e6a1268408826eb75c78b670afe079b6/blob-data/posters-enfant0-vision-2.jpg)

## GraphViz sur pmwiki

![](http://osp.constantvzw.org/api/osp.work.balsamine.2012-2013/0d8cd1def688d4d1c1287d379b53ab6ef455ca06/blob-data/screenshot_07-09-12_11-10-58.png)

## 2013–2014

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.005.jpg)

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.010.jpg)

## Des outils pour le web pour de l’impression ?

*   Un outil partagé entre les membres d'OSP à différents niveaux
*   Facile à apprendre
*   Ouvert par nature
*   Utilisation de librairies de programmation existantes
*   Approche visuelle et programmatique à la fois
*   Documents pluri-média

## De quoi avons-nous besoin?

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.016.jpg)

*   Export du PDF via une fonction native du navigateur
*   Pagination en CSS
*   Traits de coupe en javascript
*   Mélange de tons directs: possible grâce à Less CSS
*   RGB → CMJN via Ghostscript
*   …

## Problèmes

*   Couleurs en tons directs: 1 PDF noir par couche
*   Version de navigateurs ultra-spécifique: Chrome 33, Safari…

## Utilisation d'un etherpad pour le design

*   Design collectif en temps réel
*   Avoir une table des matières interactives pour naviguer dans le code ![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.011.jpg)

## Double-pages du programme

*   Utilisation d'éléments en "float" propre au HTML
*   Coupure des images en fin et début de page pour révéler le flux continu du contenu ![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.013.jpg)

## Utilisation de tableau HTML

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.014.jpg)

## Colophon

Utilisation du chaînage de texte pour l'escargot

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.015.jpg)

## 2014–2015

## Maquette dans Scribus

![](http://osp.constantvzw.org/api/osp.work.balsamine.2014-2015/fa1e6a2f3ac7242368abafaa4fb68b21313c0dae/blob-data/essai-double.png)

## Vue en planche dans le navigateur

![](http://osp.constantvzw.org/api/osp.work.balsamine.2014-2015/396dff06bd594696a92ea3de6511e9bf8f158161/blob-data/view-as-spread-in-browser.png)

## Numérotation des figures automatique

![](http://osp.kitchen/api/osp.work.balsamine.2014-2015/raw/iceberg/numbering-in-javascript.png)

## Affiche dans le navigateur

![](http://osp.kitchen/api/osp.work.balsamine.2014-2015/raw/iceberg/season-poster-in-html.png;h=cfde7309853877889c9239bef3aa448418f17434;hb=HEAD)

## Ethertoff

## Relearn

## Summer school

Relearn est une summer school organisée par OSP en 2013, par Constant en 2014\. Elle met au même niveau enseignants et étudiants; tout le monde est en situation d'apprentissage autour de thèmes de travail. Ces situations d'apprentissage puisent leur inspiration dans les logiciels libres et open source qui invitent à ce type d'échange.

*   Prise de notes collective en temps réel sur Etherpad
*   Pad difficile à lire a posteriori
*   Site généré à partir des pads (wiki)
*   Export PDF à partir du site

## Écriture en temps réel

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.017.jpg)

## Export PDF

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.018.jpg)

## Contributeurs

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.019.jpg)

## f-u-t-u-r-e.org

## Déboguage et augmentation d'Ethertoff grâce aux subsides du Cnap.

*   Pour les éditeurs, graphistes et auteurs des éditions BAT
*   Markdown: syntaxe simplifiée pour écrire du HTML
*   «Éradication des graphistes» selon les termes des graphistes de BAT

Nul besoin de back-office, toutes les méta-données sont sur le pad de chaque texte. ![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/future_write.png)

## Index automatique des pages via les métadonnées

![](http://osp.kitchen/api/osp.tools.ethertoff/e5ed65a8c42fdc11643eb997b11cd804ed047466/blob-data/future_automatic-index.png)

## Version écran

![](http://osp.kitchen/api/osp.tools.ethertoff/18b9bf96004b263882e8d8637ca7f7313db1265b/blob-data/future_screen.png)

## Version impression laser

![](http://osp.constantvzw.org/api/osp.tools.ethertoff/3e03d472b36269cd0fc989dad1cf792c0438e279/blob-data/future_laser.png)

## Aperçu impression offset dans le navigateur

![](http://osp.constantvzw.org/api/osp.tools.ethertoff/c6153a1d69859b2490fc94f468a5ad35f806d55d/blob-data/future_offset.png)

## Are You Being Served?

## Chemin de fer dans le navigateur

![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/vj14_chemin-de-fer-in-browser.png)

## Double-pages dans le navigateur

![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/vj14_spread-in-browser.png)

## Export PDF

![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/vj14_pdf.png)

## Processus éditorial

Le processus éditorial n'est plus linéaire (écriture → édition → design → relecture → corrections). Toutes ces étapes se produisent en même temps.

Comment faire du design lorsqu'on n'a pas lu le contenu qui est justement en train de se construire ? Les éditeurs doivent trouver des manières de parler du contenu aux graphistes pour pouvoir mettre en place ensemble des codes structurels et visuels.

De nouvelles modalités de processus et de collaboration doivent être trouvées.

## Ethertoff Slidy

## Mise à jour expresse d'Ethertoff pour cette présentation

Merci à Natacha Roussel pour l'idée d'utiliser Ethertoff pour faire des diaporamas.

Merci au W3C pour leur plugin Slidy ultra-facile à implémenter.

Slidy fait des diaporamas à la Power Point. La grosse différence est qu'on peut éviter le formattage impliqué par PowerPoint mais bénéficier de tous les enrichissements du Web (cf. la thèse d'Anthony Masure: [http://softphd.com](http://softphd.com)).

## Use it

*   http://osp.kitchen/tools/html2print/
*   http://osp.kitchen/tools/ethertoff/
