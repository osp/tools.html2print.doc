Title: HEAD 2015 Présentation
Date: 2017-09-13
Translation: true
Lang: fr
Tags:

## HEAD 2015 presentation

## Open Source Publishing

## [http://osp.kitchen/](http://osp.kitchen/)

We are open source publishing, a caravan based in Brussels, who only use free and open-source software to produce Graphic Design.

We started as an experiment enticed by the vibant culture in the world of free and open-source software we started to use the software they produce as an alternative to the (boring) homegenous software used in te world of Graphic Design.

Now we are not only using them but even producing them.

We prefer to call ourselves a kitchen.

## Awkward gestures

> While a familiar gesture is one that fits perfectly well in a generally accepted model, an awkward gesture is a movement that is not completely synchronic. It's not a countermovement, nor a break from the norm; it doesn’t exist outside of the pattern, nor completely in it. Like a moiré effect reveals the presence of a grid, awkward behaviour can lead to a state of increased awareness; a form of productive insecurity that presents us with openings that help understand the complex interaction between skills, tools and medium.
> 
> Femke Snelting, « Awkward Gestures », in Ludovico Alessandro, Nat Muller (éd.), Mag.net Reader, nº3, Londres, OpenMute, 2008, p. 96.

## Freedom 0

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040639rr.jpg)

## Freedom 1

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040640rr.jpg)

## Freedom 2

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040641rr.jpg)

## Freedom 3

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040642rr.jpg)

## Rule 0

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040643rr.jpg)

## Rule 1

![](http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040644rr.jpg)

## print parties

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.002.jpg)

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.003.jpg)

We do print parties in which we produce printed matter and we cook at the same time. A recipe can not be copyrighted—it can only be kept secret (like the recipe for Coca Cola) or shared (like your grand-fathers apple pie). Not only can we use some elses recipe, we can adapt it to our own version, that we in turn can pass on.

The American theorist Christopher Kelty says that Free Software over all as a set of shared and transferable practices.

## Our recipes

<iframe src="http://osp.kitchen"></iframe>

## concretely

## Théâtre la Balsamine

## 2011–2012

## Balsamine Wiki

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.009.jpg)

## Inkscape poster + Gimp image processing

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.006.jpg)

## Gantt diagrams inspired inkscape posters

![](http://osp.constantvzw.org/api/osp.work.balsamine.2011-2012/3516df3f7b4abf5b9277d70f13177fe6f8fe4192/blob-data/balsa-affiche2012-print.jpg)

## A simple page in ConTeXt

![](http://osp.constantvzw.org/api/osp.work.balsamine.2011-2012/043a5758072160d0c2d0298a5a7f7655ab2e41d5/blob-data/genese-jour4.png)

## ConTeXt specific layout model

![](http://osp.constantvzw.org/api/osp.work.balsamine.2011-2012/350e648de26f0feaab85517a217102556614cc95/blob-data/genese-page14-15.png)

## Mix ConTeXt + Scribus

![](http://stdin.fr/uploads/Works.Balsamine/arcas-back.png)

## LibreOffice Calc

![](http://osp.constantvzw.org/api/osp.work.balsamine.2011-2012/ff763721b60c9b55c3ddb708cd96cfeff004b058/blob-data/fanzine-wip.png)

## 2012–2013

## GraphViz + Inkscape

![](http://osp.constantvzw.org/api/osp.work.balsamine.2012-2013/1c5d4817e6a1268408826eb75c78b670afe079b6/blob-data/posters-enfant0-vision-2.jpg)

## GraphViz sur pmwiki

![](http://osp.constantvzw.org/api/osp.work.balsamine.2012-2013/0d8cd1def688d4d1c1287d379b53ab6ef455ca06/blob-data/screenshot_07-09-12_11-10-58.png)

## 2013–2014

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.005.jpg)

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.010.jpg)

Working on the web has made us used to a certain way of working, dynamic and in collaboration, that no traditional layout program offered—neither proprietary nor free.

The departure away from canvas based layout has many reasons (frustrations) but the benifits are multiple. It's within the very fabric of the web that we find great potential for publishing layout.

## Using HTML to print ?

*   exciting CSS specifications for print
*   increasing development of online typography
*   accessible languages: many resources
*   a tool shared by all of us at different levels
*   retro-compatible, so substainable
*   open by nature
*   an ecosystem of libraries
*   allows for a visual and programmatic approach
*   cross media possibilities

## What do we need?

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.016.jpg)

*   PDF export → via une fonction native du navigateur
*   Folios → in CSS
*   Crop marks → is javascript, or CSS
*   Spot colors mix: using Less CSS
*   RGB → CMJN via Ghostscript
*   …

## Problems

*   dependant on browsers to implement missing CSS features
*   RGB by default (we convert it with Ghostscript)
*   spot colours difficult
*   Browsers pdf rendering is a black box
*   Browsers unstable platforms

## Usinf etherpad as a collaborative design tool

*   Synchronized design
*   A table of content to structure the code

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.011.jpg)

## Spreads of the booklet

*   Using web specific layout models, like Floats.
*   Treating images as a flow ![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.013.jpg)

## Using HTML tables

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.014.jpg)

## Colophon

Using CSS regions to flow content through several containers

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.015.jpg)

## 2014–2015

## A Scribus draft layout

![](http://osp.constantvzw.org/api/osp.work.balsamine.2014-2015/fa1e6a2f3ac7242368abafaa4fb68b21313c0dae/blob-data/essai-double.png)

## Spread view in the navigator

![](http://osp.constantvzw.org/api/osp.work.balsamine.2014-2015/396dff06bd594696a92ea3de6511e9bf8f158161/blob-data/view-as-spread-in-browser.png)

## Automatic numbering of the figures

![](http://osp.kitchen/api/osp.work.balsamine.2014-2015/raw/iceberg/numbering-in-javascript.png)

## A poster in the browser

![](http://osp.kitchen/api/osp.work.balsamine.2014-2015/raw/iceberg/season-poster-in-html.png)

## Ethertoff

## Relearn

## Summer school

Relearn is a summerschool which welcomes persons, artists, students, teachers from all backgrounds and disciplines. Participants will gather to learn from and teach to each other, beyond the traditional paradigms of education.

*   Collaborative note taking using etherpads
*   Site generated from the pads (wiki)
*   PDF export from the site itself

## Writing in real-time

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.017.jpg)

## PDF export

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.018.jpg)

## Contributors

![](http://silo.schr.fr/kabk14/PRESENTATIE_OSP_PROJECTWEEK2014.019.jpg)

## f-u-t-u-r-e.org

## Augmenting Ethertoff

*   To be used by the collaborators, the editors, and the graphic designers
*   Using a simplified markup language to produce HTML
*   «Eradicating des graphistes» zccording to the graphic designer of BAT

Schema-free metadata ![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/future_write.png)

## Automatic indexing of metadata

![](http://osp.kitchen/api/osp.tools.ethertoff/e5ed65a8c42fdc11643eb997b11cd804ed047466/blob-data/future_automatic-index.png)

## On-screen version

![](http://osp.kitchen/api/osp.tools.ethertoff/18b9bf96004b263882e8d8637ca7f7313db1265b/blob-data/future_screen.png)

## Laser printing version

![](http://osp.constantvzw.org/api/osp.tools.ethertoff/3e03d472b36269cd0fc989dad1cf792c0438e279/blob-data/future_laser.png)

## Color seperation preview

![](http://osp.constantvzw.org/api/osp.tools.ethertoff/c6153a1d69859b2490fc94f468a5ad35f806d55d/blob-data/future_offset.png)

## Are You Being Served?

## flatplan

![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/vj14_chemin-de-fer-in-browser.png)

## Spreads

![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/vj14_spread-in-browser.png)

## PDF export

![](http://osp.kitchen/api/osp.tools.ethertoff/raw/iceberg/vj14_pdf.png)

## Workshop @ HEAR Strasbourg

## Workshop @ HEAR Strasbourg

*   As an example of collaboration
*   Experimenting what would be media queries for print.
*   What styles are shared? What styles are specific to a format?
*   Feel how flow design is different from visual design

## Responsive print?

![Imgur](http://i.imgur.com/yuB33Yv.jpg)
